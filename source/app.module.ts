import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';

import { appRouting } from './app.routing';
import { AppComponent } from './app.component';
import { APP_REDUCERS, APP_INIT } from './data/data.store';

@NgModule({
	imports: [
		BrowserModule,
		appRouting,
		StoreModule.provideStore(APP_REDUCERS, APP_INIT),
	],
	declarations: [
		AppComponent,
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
