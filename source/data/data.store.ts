import { selectedUserReducer } from './selectedUser/selectedUser.reducer';

import { IUser } from '../users/data/users/user';

export { IUser };
export * from './selectedUser/selectUserActions';

export interface IAppState {
	selectedUser: IUser;
}

export const APP_REDUCERS = {
	selectedUser: selectedUserReducer,
};

export const APP_INIT: IAppState = {
	selectedUser: null,
};
