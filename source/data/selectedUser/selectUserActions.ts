import { Action } from '@ngrx/store';

import { IUser } from '../../users/data/users/user';

export const SELECT_USER = 'SELECT_USER';

const selectUserAction = user => {
	return {
		type: SELECT_USER,
		payload: user,
	};
};

export interface ISelectUserActionFactory {
	selectUser(user: IUser): Action;
}

export const selectUserActionFactory: ISelectUserActionFactory = {
	selectUser: user => selectUserAction(user),
};
