import { ActionReducer, Action } from '@ngrx/store';

import { IUser } from '../../users/data/users/user';
import { SELECT_USER } from './selectUserActions';

export const selectedUserReducer: ActionReducer<IUser> = (state: IUser = null, action: Action) => {
	switch (action.type) {
		case SELECT_USER:
			if (state === action.payload) {
				return null;
			} else {
				return action.payload;
			}

		default:
			return state;
	}
};
