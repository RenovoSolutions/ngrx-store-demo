import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
	{
		path: '',
		redirectTo: '/users',
		pathMatch: 'full'
	},
	{
		path: 'users',
		loadChildren: 'source/users/users.module#UsersModule',
	},
	{
		path: 'stories',
		loadChildren: 'source/stories/stories.module#StoriesModule',
	},
];

export const appRouting = RouterModule.forRoot(appRoutes);
