export interface IResource {
	get?: { (): any };
	put?: { (data: any): any };
	post?: { (data: any): any };
	delete?: { (): void };
}

export class SimulatedBackendService {
	dictionary: { [key: string]: any };

	getResource(key: string): IResource {
		const getSlashFollowedByNumber = /(\/\d+)/;
		key = key.replace(getSlashFollowedByNumber, '');
		const resource = this.dictionary[key];

		if (!resource) {
			throw new Error(`No resource defined for ${key}`);
		}

		return resource;
	}

	constructor() {
		const users = [
			{ id: 1, name: 'SamGraber', displayName: 'Sam Graber' },
		];

		const stories = [
			{ id: 1, summary: 'A cool story', author: users[0] },
		];

		this.dictionary = {
			'api/users': {
				get: () => users,
				post: user => user,
				put: user => user,
				delete: () => null,
			},
			'api/stories': {
				get: () => stories,
				post: story => story,
				put: story => story,
				delete: () => null,
			},
		};
	}
}