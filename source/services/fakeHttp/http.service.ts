import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { SimulatedBackendService, IResource } from './simulatedBackend.service';

export const DEFAULT_HTTP_DELAY = 1000;

@Injectable()
export class HttpService {
	backend: SimulatedBackendService;

	constructor(backend: SimulatedBackendService) {
		this.backend = backend;
	}

	get<T>(url: string, params?: any): Observable<T> {
		const resource = this.backend.getResource(url);

		if (!resource.get) {
			throw new Error(`Get is not supported on ${url}`);
		}

		return Observable.of(resource.get()).delay(DEFAULT_HTTP_DELAY);
	}

	post<T>(url: string, data?: any): Observable<T> {
		const resource = this.backend.getResource(url);

		if (!resource.post) {
			throw new Error(`Post is not supported on ${url}`);
		}

		return Observable.of(resource.post(data))
			.delay(DEFAULT_HTTP_DELAY)
			.do(() => console.log(`Posted ${data} to ${url}`));
	}

	put<T>(url: string, data?: any): Observable<T> {
		const resource = this.backend.getResource(url);

		if (!resource.put) {
			throw new Error(`Put is not supported on ${url}`);
		}

		return Observable.of(resource.put(data))
			.delay(DEFAULT_HTTP_DELAY)
			.do(() => console.log(`Put ${data} to ${url}`));
	}

	delete<T>(url: string): Observable<void> {
		const resource = this.backend.getResource(url);

		if (!resource.delete) {
			throw new Error(`Delete is not supported on ${url}`);
		}

		return Observable.of(resource.delete())
			.delay(DEFAULT_HTTP_DELAY)
			.do(() => console.log(`Deleted ${url}`));
	}
}
