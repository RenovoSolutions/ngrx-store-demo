import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { storiesRouting } from './stories.routing';
import { StoriesComponent } from './stories.component';
import { StoryListComponent } from './components/storyList/storyList.component';
import { StoryFormComponent } from './components/storyForm/storyForm.component';
import { SimulatedBackendService } from '../services/fakeHttp/simulatedBackend.service';
import { HttpService } from '../services/fakeHttp/http.service';
import { StoriesService } from './data/stories/stories.service';
import { STORIES_REDUCERS, STORIES_INIT, IStoriesState } from './data/data.store';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		storiesRouting,
	],
	declarations: [
		StoriesComponent,
		StoryListComponent,
		StoryFormComponent,
	],
	providers: [
		SimulatedBackendService,
		HttpService,
		StoriesService,
	],
})
export class StoriesModule {
	constructor(store: Store<IStoriesState>) {
		store.replaceReducer(combineReducers(STORIES_REDUCERS));
	}
}
