import { storiesReducer } from './stories/stories.reducer';

import { IStoryBranch } from './stories/story';

export { IStoryBranch };

export interface IStoriesState {
	stories: IStoryBranch[];
}

export const STORIES_REDUCERS = {
	stories: storiesReducer,
};

export const STORIES_INIT: IStoriesState = {
	stories: [],
};
