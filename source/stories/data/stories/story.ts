import { IUser } from '../../../users/data/users/user';

export { IUser };

export interface IStoryBranch {
	id: number;
	summary: string;
	author: IUser;
}
