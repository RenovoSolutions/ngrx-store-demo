import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { storyActionFactory } from './stories.actions';
import { IStoriesState, IStoryBranch } from '../data.store';
import { HttpService } from '../../../services/fakeHttp/http.service';

@Injectable()
export class StoriesService {
	stories: Observable<IStoryBranch[]>;

	store: Store<IStoriesState>;
	http: HttpService;

	constructor(store: Store<IStoriesState>, http: HttpService) {
		this.store = store;
		this.http = http;
		this.stories = store.select(app => app.stories);
		this.loadItems();
	}

	loadItems(): void {
		this.http.get<IStoryBranch[]>('api/stories')
			.map(stories => storyActionFactory.loadStories(stories))
			.subscribe(action => this.store.dispatch(action));
	}

	create(user: IStoryBranch): Observable<IStoryBranch> {
		const request = this.http.post<IStoryBranch>('api/stories', user);
		request.map(newUser => storyActionFactory.addStory(newUser))
			.subscribe(action => this.store.dispatch(action));
		return request;
	}

	update(newUser: IStoryBranch, oldUser: IStoryBranch): Observable<IStoryBranch> {
		const request = this.http.put<IStoryBranch>(`api/stories/${oldUser.id}`, newUser);
		request.map(updatedUser => storyActionFactory.updateStory(oldUser, updatedUser))
			.subscribe(action => this.store.dispatch(action));
		return request;
	}

	delete(user: IStoryBranch): Observable<void> {
		const request = this.http.delete(`api/stories/${user.id}`);
		request.map(() => storyActionFactory.deleteStory(user))
			.subscribe(action => this.store.dispatch(action));
		return request;
	}
}
