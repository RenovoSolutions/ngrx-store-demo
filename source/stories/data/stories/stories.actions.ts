import { Action } from '@ngrx/store';

import { IStoryBranch } from './story';

export const ADD_STORY = 'ADD_STORY';
export const LOAD_STORIES = 'LOAD_STORIES';
export const UPDATE_STORY = 'UPDATE_STORY';
export const DELETE_STORY = 'DELETE_STORY';

const loadStoriesAction = stories => {
	return {
		type: LOAD_STORIES,
		payload: stories,
	};
};

const addStoryAction = story => {
	return {
		type: ADD_STORY,
		payload: story,
	};
};

const updateStoryAction = (oldStory, newStory) => {
	return {
		type: UPDATE_STORY,
		payload: { oldStory, newStory },
	};
};

const deleteStoryAction = story => {
	return {
		type: DELETE_STORY,
		payload: story,
	};
};

export interface IStoryActionFactory {
	loadStories(stories: IStoryBranch[]): Action;
	addStory(story: IStoryBranch): Action;
	updateStory(oldStory: IStoryBranch, newStory: IStoryBranch): Action;
	deleteStory(story: IStoryBranch): Action;
}

export const storyActionFactory: IStoryActionFactory = {
	loadStories: stories => loadStoriesAction(stories),
	addStory: story => addStoryAction(story),
	updateStory: (oldStory, newStory) => updateStoryAction(oldStory, newStory),
	deleteStory: story => deleteStoryAction(story),
};
