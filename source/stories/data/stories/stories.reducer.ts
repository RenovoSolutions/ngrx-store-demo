import { ActionReducer, Action } from '@ngrx/store';
import { without, map, maxBy } from 'lodash';

import { IStoryBranch } from './story';

import { LOAD_STORIES, ADD_STORY, UPDATE_STORY, DELETE_STORY } from './stories.actions';

export const storiesReducer: ActionReducer<IStoryBranch[]> = (state: IStoryBranch[] = [], action: Action) => {
	switch (action.type) {
		case ADD_STORY:
			const newId = maxBy(state, user => user.id).id + 1;
			action.payload.id = newId;
			return [...state, action.payload];

		case LOAD_STORIES:
			return [...action.payload];

		case UPDATE_STORY:
			return map(state, user => {
				return user === action.payload.oldUser
					? action.payload.newUser
					: user;
			});

		case DELETE_STORY:
			return without(state, action.payload);

		default:
			return state;
	}
};
