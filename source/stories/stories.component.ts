import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Scheduler } from 'rxjs';

import { IAppState } from '../data/data.store';
import { IStoriesState, IStoryBranch } from './data/data.store';
import { StoriesService } from './data/stories/stories.service';

@Component({
	moduleId: module.id,
	selector: 'rxStoriesView',
	templateUrl: 'stories.component.html',
})
export class StoriesComponent {
	stories: Observable<IStoryBranch[]>;

	store: Store<IAppState>;
	storiesService: StoriesService;

	constructor(appStore: Store<IAppState>, storiesService: StoriesService) {
		this.store = appStore;
		this.storiesService = storiesService;
		this.stories = storiesService.stories;
	}

	remove(story: IStoryBranch): void {
		this.storiesService.delete(story);
	}

	add(story: IStoryBranch): void {
		const subscription = this.store.select(x => x.selectedUser).subscribeOn(Scheduler.async).subscribe(author => {
			story.author = author;
			this.storiesService.create(story);
		});
	}
}
