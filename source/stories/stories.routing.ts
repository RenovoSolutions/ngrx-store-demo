import { Routes, RouterModule } from '@angular/router';

import { StoriesComponent } from './stories.component';

const storiesRoutes: Routes = [
	{
		path: '',
		component: StoriesComponent,
	},
];

export const storiesRouting = RouterModule.forChild(storiesRoutes);
