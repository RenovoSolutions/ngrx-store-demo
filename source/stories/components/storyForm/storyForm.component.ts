import { Component, Input, Output, EventEmitter } from '@angular/core';
import { clone } from 'lodash';

import { IStoryBranch } from '../../data/data.store';

@Component({
	moduleId: module.id,
	selector: 'rxStoryForm',
	templateUrl: 'storyForm.component.html',
})
export class StoryFormComponent {
	@Output() storyChange: EventEmitter<IStoryBranch> = new EventEmitter<IStoryBranch>();

	@Input() set story(value: IStoryBranch) {
		this.updatedStory = clone(value) || <any>{};
	}

	updatedStory: IStoryBranch;

	constructor() {
		this.updatedStory = <any>{};
	}

	save(): void {
		this.storyChange.emit(this.updatedStory);
	}
}
