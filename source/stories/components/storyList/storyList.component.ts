import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

import { IStoryBranch } from '../../data/data.store';

@Component({
	moduleId: module.id,
	selector: 'rxStoryList',
	templateUrl: 'storyList.component.html',
})
export class StoryListComponent {
	@Input() stories: Observable<IStoryBranch[]>;
	@Output() remove: EventEmitter<IStoryBranch> = new EventEmitter<IStoryBranch>();
}
