import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { IAppState, IUser } from './data/data.store';

@Component({
	moduleId: module.id,
	selector: 'my-app',
	templateUrl: 'app.component.html',
})
export class AppComponent {
	selectedUser: Observable<IUser>;

	store: Store<IAppState>;

	constructor(store: Store<IAppState>) {
		this.store = store;
		this.selectedUser = store.select(x => x.selectedUser);
	}
}
