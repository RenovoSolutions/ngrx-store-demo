import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { userRouting } from './users.routing';
import { UsersComponent } from './users.component';
import { UserListComponent } from './components/userList/userList.component';
import { UserFormComponent } from './components/userForm/userForm.component';
import { SimulatedBackendService } from '../services/fakeHttp/simulatedBackend.service';
import { HttpService } from '../services/fakeHttp/http.service';
import { UsersService } from './data/users/users.service';
import { USERS_REDUCERS, IUsersState } from './data/data.store';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		userRouting,
	],
	declarations: [
		UsersComponent,
		UserListComponent,
		UserFormComponent,
	],
	providers: [
		SimulatedBackendService,
		HttpService,
		UsersService,
	],
})
export class UsersModule {
	constructor(store: Store<IUsersState>) {
		store.replaceReducer(combineReducers(USERS_REDUCERS));
	}
}
