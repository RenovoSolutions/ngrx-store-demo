import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Scheduler } from 'rxjs';

import { IAppState } from '../data/data.store';
import { IUsersState, IUser } from './data/data.store';
import { selectUserActionFactory } from '../data/selectedUser/selectUserActions';
import { UsersService } from './data/users/users.service';

@Component({
	moduleId: module.id,
	selector: 'rxUsersView',
	templateUrl: 'users.component.html',
})
export class UsersComponent {
	users: Observable<IUser[]>;
	selectedUser: Observable<IUser>;

	store: Store<IUsersState>;
	appStore: Store<IAppState>;
	userService: UsersService;

	constructor(store: Store<IUsersState>, userService: UsersService) {
		this.store = store;
		this.userService = userService;
		this.appStore = <any>store;
		this.users = userService.users;
		this.selectedUser = this.appStore.select(x => x.selectedUser);
	}

	remove(user: IUser): void {
		this.userService.delete(user);
	}

	add(user: IUser): void {
		this.userService.create(user);
		this.select(user);
	}

	update(newUser: IUser): void {
		const subscription = this.selectedUser.subscribeOn(Scheduler.async).subscribe(oldUser => {
			this.userService.update(newUser, oldUser);
			subscription.unsubscribe();
			this.select(newUser);
		});
	}

	select(user: IUser): void {
		this.appStore.dispatch(selectUserActionFactory.selectUser(user));
	}
}
