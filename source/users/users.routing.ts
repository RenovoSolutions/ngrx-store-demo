import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';

const userRoutes: Routes = [
	{
		path: '',
		component: UsersComponent,
	},
];

export const userRouting = RouterModule.forChild(userRoutes);
