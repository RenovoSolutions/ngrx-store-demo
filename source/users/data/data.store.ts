import { userReducer } from './users/users.reducer';

import { IUser } from './users/user';

export * from './users/user';
export * from './users/users.service';

export interface IUsersState {
	users: IUser[];
}

export const USERS_REDUCERS = {
	users: userReducer,
};

export const USERS_INIT: IUsersState = {
	users: [],
};
