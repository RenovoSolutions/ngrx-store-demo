import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { userActionFactory } from './userActions';
import { IUsersState, IUser } from '../data.store';
import { HttpService } from '../../../services/fakeHttp/http.service';

@Injectable()
export class UsersService {
	users: Observable<IUser[]>;

	store: Store<IUsersState>;
	http: HttpService;

	constructor(store: Store<IUsersState>, http: HttpService) {
		this.store = store;
		this.http = http;
		this.users = store.select(app => app.users);
		this.loadItems();
	}

	loadItems(): void {
		this.http.get<IUser[]>('api/users')
			.map(users => userActionFactory.loadUsers(users))
			.subscribe(action => this.store.dispatch(action));
	}

	create(user: IUser): Observable<IUser> {
		const request = this.http.post<IUser>('api/users', user);
		request.map(newUser => userActionFactory.addUser(newUser))
			.subscribe(action => this.store.dispatch(action));
		return request;
	}

	update(newUser: IUser, oldUser: IUser): Observable<IUser> {
		const request = this.http.put<IUser>(`api/users/${oldUser.id}`, newUser);
		request.map(updatedUser => userActionFactory.updateUser(oldUser, updatedUser))
			.subscribe(action => this.store.dispatch(action));
		return request;
	}

	delete(user: IUser): Observable<void> {
		const request = this.http.delete(`api/users/${user.id}`);
		request.map(() => userActionFactory.deleteUser(user))
			.subscribe(action => this.store.dispatch(action));
		return request;
	}
}
