import { Action } from '@ngrx/store';

import { IUser } from './user';

export const ADD_USER = 'ADD_USER';
export const LOAD_USERS = 'LOAD_USERS';
export const UPDATE_USER = 'UPDATE_USER';
export const DELETE_USER = 'DELETE_USER';

const loadUsersAction = users => {
	return {
		type: LOAD_USERS,
		payload: users,
	};
};

const addUserAction = user => {
	return {
		type: ADD_USER,
		payload: user,
	};
};

const updateUserAction = (oldUser, newUser) => {
	return {
		type: UPDATE_USER,
		payload: { oldUser, newUser },
	};
};

const deleteUserAction = user => {
	return {
		type: DELETE_USER,
		payload: user,
	};
};

export interface IUserActionFactory {
	loadUsers(users: IUser[]): Action;
	addUser(user: IUser): Action;
	updateUser(oldUser: IUser, newUser: IUser): Action;
	deleteUser(user: IUser): Action;
}

export const userActionFactory: IUserActionFactory = {
	loadUsers: users => loadUsersAction(users),
	addUser: user => addUserAction(user),
	updateUser: (oldUser, newUser) => updateUserAction(oldUser, newUser),
	deleteUser: user => deleteUserAction(user),
};
