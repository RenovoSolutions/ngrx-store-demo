import { ActionReducer, Action } from '@ngrx/store';
import { without, map, maxBy } from 'lodash';

import { IUser } from './user';

import { ADD_USER, LOAD_USERS, UPDATE_USER, DELETE_USER } from './userActions';

export const userReducer: ActionReducer<IUser[]> = (state: IUser[] = [], action: Action) => {
	switch (action.type) {
		case ADD_USER:
			const newId = maxBy(state, user => user.id).id + 1;
			action.payload.id = newId;
			return [...state, action.payload];

		case LOAD_USERS:
			return [...action.payload];

		case UPDATE_USER:
			return map(state, user => {
				return user === action.payload.oldUser
					? action.payload.newUser
					: user;
			});

		case DELETE_USER:
			return without(state, action.payload);

		default:
			return state;
	}
};
