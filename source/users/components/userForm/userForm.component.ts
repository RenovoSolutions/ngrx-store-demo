import { Component, Input, Output, EventEmitter } from '@angular/core';
import { clone } from 'lodash';

import { IUser } from '../../data/data.store';

@Component({
	moduleId: module.id,
	selector: 'rxUserForm',
	templateUrl: 'userForm.component.html',
})
export class UserFormComponent {
	@Output() userChange: EventEmitter<IUser> = new EventEmitter<IUser>();

	@Input() set user(value: IUser) {
		this.updatedUser = clone(value) || <any>{};
	}

	updatedUser: IUser;

	constructor() {
		this.updatedUser = <any>{};
	}

	save(): void {
		this.userChange.emit(this.updatedUser);
	}
}
