import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

import { IUser } from '../../data/data.store';

@Component({
	moduleId: module.id,
	selector: 'rxUserList',
	templateUrl: 'userList.component.html',
})
export class UserListComponent {
	@Input() users: Observable<IUser[]>;
	@Output() remove: EventEmitter<IUser> = new EventEmitter<IUser>();
	@Output() select: EventEmitter<IUser> = new EventEmitter<IUser>();

	delete(user: IUser, $event: MouseEvent): void {
		this.remove.emit(user)
		$event.stopPropagation();
	}
}
